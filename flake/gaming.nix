{config, pkgs, ...}:

{
  environment = {                               
    systemPackages = with pkgs; [
      ## Gaming
      steam
      # itch -- Has security issues, still using 11.5.x electron version, I will put it back if it is fixed or wanna play something from itch
      lutris

      ## PS remote play
      chiaki
      android-studio
    ];
  };

  programs.adb.enable = true;
  users.users.sedon.extraGroups = ["adbusers"];

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };


}
