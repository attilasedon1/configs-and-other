{config, pkgs, userJudit, ...}:
{
  home.username = userJudit;
  home.homeDirectory = "/home/${userJudit}";
  home.packages = with pkgs; [                               
    # Browser
    firefox
    # Music
    spotify
  ];

}
