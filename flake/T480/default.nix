{config, pkgs, ...}:

{
  imports = [ ./hardware-configuration.nix ];
  networking.hostName = "T480";
users.users.judit = {
  isNormalUser = true;
  description = "Judit";
  extraGroups = [ "networkmanager" "video" "libvirt" "kvm" "libvirtd"];
};
system.stateVersion = "24.05";
}
