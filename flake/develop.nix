{config, pkgs, ...}:

{
  environment.systemPackages = with pkgs; [
      jetbrains.idea-ultimate
      jetbrains.webstorm
      jetbrains.clion
      ## Haskell
      ghc
      cabal-install
      stack
      haskell-language-server 

      ## Go
      go
      ### collection of tools and libs, including static check and linters
      go-tools
      ### Go security checker
      gosec
      ### go language server
      gopls
      ## DB migration tool
      goose
      ## SQL to go code generator
      sqlc

      ## Rust
      rustup

      ## Node.js
      nodejs
      nodePackages.typescript-language-server
      nodePackages.typescript
      nodePackages.yarn
      nodePackages.npm

      # Android development
      cordova
      android-tools
      android-studio
      gradle

      ## PureScript
      purescript

      ## Dart
      dart

      ## Java
      jdk

      ## Python
      python3
      ## Postman

      ## Trying new distros
      podman
      docker
      distrobox
      gnome.gnome-boxes
    ];
  }
