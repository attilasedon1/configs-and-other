{config, pkgs, ...}:
  {
    environment = {                               
      systemPackages = with pkgs; [
        file-roller

        gnome.gnome-tweaks
        transmission_4-gtk
      ];
    };
          services.xserver = {
            displayManager = {
              gdm.enable = true;
            };
            desktopManager.gnome.enable = true;
          };
          environment.gnome.excludePackages = (with pkgs; [
            gnome-tour
            gedit # text editor
            gnome-terminal
            epiphany # web browser
            geary # email reader
            tali # poker game
            iagno # go game
            hitori # sudoku game
            atomix # puzzle game
          ]);

    }
