{pkgs, ...}: 
{
  programs.hyprland = {
    # Install the packages from nixpkgs
    enable = true;
    # Whether to enable XWayland
    xwayland.enable = true;

    # Optional
    # Whether to enable patching wlroots for better Nvidia support
    enableNvidiaPatches = true;
    extraPackages = with pkgs; [
      mako
      libsForQt5.polkit-kde-agent
      gnome.file-roller
      gnome.gnome-tweaks
      wofi
      hyprpaper
      hyprpicker
      clipman
    ]
  };

    security.polkit.enable = true;  
    services.greetd = {
      enable = true;
      settings = {
       default_session.command = ''
        ${pkgs.greetd.tuigreet}/bin/tuigreet \
          --time \
          --asterisks \
          --user-menu \
          --cmd hyprland
      '';
      };
    };

    environment.etc."greetd/environments".text = ''
      hyprland
    '';

    services.dbus.enable = true;
      xdg.portal = {
      enable = true;
      wlr.enable = true;
      # gtk portal needed to make gtk apps happy
      extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };

     programs.waybar.enable = true;
  # ...
}
