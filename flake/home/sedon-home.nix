{config, pkgs, userSedon, emailSedon, ...}: 
{
  home.username = userSedon;
  home.homeDirectory = "/home/${userSedon}";

  # Packages that should be installed to the user profile.
  home.packages = with pkgs; [

    # Fun terminal commands
    sl
    lolcat
    cowsay
    hyfetch
    hollywood
    toilet
    fortune
    asciiquarium

    ## Proton
    protonvpn-gui

    # nextcloud
    nextcloud-client

    ## Bitwarden
    bitwarden
    bitwarden-cli

    ## File transfer
    filezilla

    ## Browser
    firefox

    ## Reach server
    cloudflared
    remmina

    # Ventoy
    ventoy-bin

    # Fonts
    #nerdfonts
    iosevka
    arkpandora_ttf
    corefonts
    font-awesome

    # Ricing
    dracula-theme
    beauty-line-icon-theme
    nordic
    nordzy-cursor-theme
    nordzy-icon-theme
    sweet
  ];


  programs.ssh = {
    enable = true;
    extraConfig = ''
      Host ssh.sedonattila.dev
        ProxyCommand ${pkgs.cloudflared}/bin/cloudflared access ssh --hostname %h
    '';
  };

  # Editor
  programs.emacs = {
    enable = true;
    package = pkgs.emacs;
    extraPackages = epkgs: [ epkgs.pdf-tools epkgs.org-pdftools ]; # non-trivial
  };

  services.emacs = {
    enable = true;
    package = pkgs.emacs;
  };

  programs.git = {
      enable = true;
      userName = "Attila Sedon";
      userEmail = emailSedon;
    };

}
