{config, pkgs, ...}:
  {
    environment = {                               
      systemPackages = with pkgs; [
        file-roller
        pavucontrol
        xfce.xfce4-pulseaudio-plugin
        xfce.xfce4-whiskermenu-plugin
        xfce.xfce4-panel
      ];
    };
  services.xserver = {
    enable = true;
    displayManager = {
      defaultSession = "xfce";
    };
    desktopManager.xfce = {
      enable = true;
    };

  };

  programs.thunar.plugins = with pkgs.xfce; [
    thunar-archive-plugin
    thunar-volman
  ];
}
