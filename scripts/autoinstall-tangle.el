(print "Started Auto tangle")
;; Setup system
(require 'org)
(let ((config-directory "~/Projects/configs-and-other/"))
 (mapcar (lambda (file) (org-babel-tangle-file (expand-file-name (concat config-directory file)))) (directory-files config-directory nil "\\.org$"))
 )
