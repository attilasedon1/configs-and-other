{config, pkgs, ...}:

{
  home.packages = with pkgs; [
      zoom-us

      ## Haskell
      ghc
      cabal-install
      stack
      haskell-language-server 

      ## Go
      go
      ### collection of tools and libs, including static check and linters
      go-tools
      ### Go security checker
      gosec
      ### go language server
      gopls
      ## DB migration tool
      goose
      ## SQL to go code generator
      sqlc
      ## Live reload for go servers
      air

      ## Hugo
      hugo

      ## Rust
      # rustup
      rustc
      cargo
      cargo-generate
      clippy
      rustfmt
      rust-analyzer

      ## Node.js
      nodejs
      nodePackages.typescript-language-server
      nodePackages.eslint
      nodePackages.typescript
      nodePackages.yarn
      nodePackages.npm
      nodePackages.eas-cli
      tailwindcss
      rustywind ## tailwind class sorter

      ## OCaml
      ocaml
      opam
      dune_3
      ocamlPackages.ocaml-lsp
      ocamlPackages.ocamlformat

      ## Java
      jdk

      ## Python
      python3
      ## Postman

      ## Trying new distros
      podman
      docker
      distrobox
      gnome-boxes
      qemu
    ];
  }
