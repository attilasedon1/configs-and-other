{config, pkgs, ...}:
{

  environment.systemPackages = with pkgs; [
      kdePackages.ktorrent
  ];
  
  services = {
    displayManager = {
      sddm.enable = true;
    };
    xserver = {
      desktopManager.lxqt.enable = true;
    };
  };

}
