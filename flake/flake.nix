{
  description = "Personal Flake";
  inputs = {

    nixpkgs.url = "github:NixOS/nixpkgs?ref=nixos-24.11";


    home-manager = {
      url = "github:nix-community/home-manager?ref=release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

  };
  outputs = { self, nixpkgs, home-manager }: 
    let 
      system = "x86_64-linux";
      userSedon = "sedon";
      userJudit = "judit";
      emailSedon="attila.sedon@sedonattila.com";
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
      lib = nixpkgs.lib;
    in {
      nixosConfigurations = {
        asus-rog = lib.nixosSystem {
          inherit system;
          specialArgs = {
            inherit userSedon;
            host = {
              hostName="asus-rog";
            };
          };
          modules = [ 
            ./base-configuration.nix
            ./gaming.nix
            ./asus-rog
            ./desktop/kde.nix
            home-manager.nixosModules.home-manager {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.extraSpecialArgs = {
                inherit userSedon emailSedon;
                host = {
                  hostName="asus-rog";
                };
              };
              home-manager.users.${userSedon} = {
                imports = [
                  ./home/base-home.nix
                  ./home/sedon-home.nix
                  ./home/develop-home.nix
                ];
              };
            }
          ];
        };

        T480 = lib.nixosSystem {
          inherit system;
          specialArgs = {
            inherit userSedon userJudit;
            host = {
              hostName="T480";
            };
          };
          modules = [
            ./base-configuration.nix
            ./gaming.nix
            ./T480
            ./desktop/gnome.nix
            home-manager.nixosModules.home-manager {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.extraSpecialArgs = {
                inherit userSedon userJudit emailSedon;
                host = {
                  hostName="T480";
                };
              };
              home-manager.users.${userSedon} = {
                imports = [
                  ./home/develop-home.nix
                  ./home/base-home.nix
                  ./home/sedon-home.nix
                ];
              };
              home-manager.users.${userJudit} = {
                imports = [
                  ./home/base-home.nix
                  ./home/judit-home.nix
                ];
              };
            }
          ];
        };


        X230 = lib.nixosSystem {
          inherit system;
          specialArgs = {
            inherit userSedon;
            host = {
              hostName="X230";
            };
          };
          modules = [
            ./base-configuration.nix
            ./desktop/kde.nix
            ./X230
            home-manager.nixosModules.home-manager {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.extraSpecialArgs = {
                inherit userSedon emailSedon;
                host = {
                  hostName="X230";
                };
              };
              home-manager.users.${userSedon} = {
                imports = [
                  ./home/develop-home.nix
                  ./home/base-home.nix
                  ./home/sedon-home.nix
                ];
              };
            }
          ];
        };

        server = lib.nixosSystem {
          inherit system;
          specialArgs = {
            inherit userSedon;
            host = {
              hostName="server";
            };
          };
          modules = [
            ./base-configuration.nix
            ./server
            ./desktop/kde.nix
            home-manager.nixosModules.home-manager {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.extraSpecialArgs = {
                inherit userSedon emailSedon;
                host = {
                  hostName="server";
                };
              };
              home-manager.users.${userSedon} = {
                imports = [
                  ./home/base-home.nix
                  ./home/sedon-home.nix
                  ./home/develop-home.nix
                ];
              };
            }
          ];
        };
      };
    };
}
