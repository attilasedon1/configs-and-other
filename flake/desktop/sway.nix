{config, pkgs, ...}:
  {
        programs.sway = {
        wrapperFeatures.gtk = true; # so that gtk works properly
        extraPackages = with pkgs; [
          swaylock
          swayidle
          swaynag-battery
          wl-clipboard
          wf-recorder
          mako # notification daemon
          slurp
          wofi
          waybar
          libappindicator # needed for waybar
          pamixer
          pavucontrol
          autotiling
          sway-contrib.grimshot
          gnome.file-roller

          gnome.gnome-tweaks

          ## File manager
          gnome.nautilus

          ## Bluetooth
          bluez
          bluez-tools
          blueberry
          # Screenshot
          gnome.gnome-screenshot
          auto-cpufreq
          tlp
        ];
        extraSessionCommands = ''
          export SDL_VIDEODRIVER=wayland
          export QT_QPA_PLATFORM=wayland
          export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
          export _JAVA_AWT_WM_NONREPARENTING=1
          export XDG_CURRENT_DESKTOP=Unity
        '';
       };
       programs.waybar.enable = true;
       programs.sway.enable = true;
       services = {
         displayManager = {
          sddm.enable = true;
          sddm.wayland.enable = true;
         };
       };


  services.auto-cpufreq.enable = true;
  services.tlp.enable = true;


  systemd.services.swaynag-battery = {
    wantedBy = [ "sway-session.target" ];
    serviceConfig = {
      Type="simple";
      ExecStart = ''
        ${pkgs.swaynag-battery}
        '';
      Restart = "always";
    };
  };

    }
