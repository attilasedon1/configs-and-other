{config, pkgs, ...}:

let 
  openrgb-rules = builtins.fetchurl {
    url = "https://gitlab.com/attilasedon1/configs-and-other/-/raw/main/openrgb.rules";
    sha256 = "06hli39dwbm77cfnmpl3bvp0cxqpw4l2w2g12rnk4bxgvvn18dpf";
  };	
in  {
  imports = [ ./hardware-configuration.nix ];
  networking.hostName = "asus-rog";
  boot.kernelModules = [ "i2c-dev" "i2c-piix4" ];
  boot.initrd.kernelModules = ["amdgpu"];
  boot.loader.grub.extraConfig = "set theme=ROG/theme.txt";
  boot.loader.grub.gfxmodeEfi = "auto";
  boot.loader.grub.gfxmodeBios = "auto";
  boot.loader.grub.splashImage = "/usr/share/ROG-simple-background.png";
  boot.loader.efi.efiSysMountPoint = "/boot/efi";
  services.udev.extraRules =  builtins.readFile openrgb-rules;
  environment = {                               # Packages installed system wide
    systemPackages = with pkgs; [               # This is because some options need to be configured. 
      ## RGB lighting
      openrgb
      i2c-tools
      libva-utils
    ];
  };
  services.xserver.videoDrivers = [ "amdgpu" ];
  hardware.graphics.extraPackages = with pkgs; [
    libvdpau-va-gl
    vaapiVdpau
  ];

  environment.variables = {
  # VAAPI and VDPAU config for accelerated video.
  # See https://wiki.archlinux.org/index.php/Hardware_video_acceleration
  "VDPAU_DRIVER" = "radeonsi";
  "LIBVA_DRIVER_NAME" = "radeonsi";
  };
  system.stateVersion = "22.05";
}
