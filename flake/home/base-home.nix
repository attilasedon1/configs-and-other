{config, pkgs, ...}: 
{

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "24.11";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;


  fonts.fontconfig.enable = true;

  home.packages = with pkgs; [
    wl-clipboard

    # Applications
    ## Torrent  
    transmission_4

    ## Photo editing
    #digikam
    pinta
    inkscape

    ## Office suite
    libreoffice


    ## Video player
    vlc
    ffmpeg

    ## OS Maintanance
    bleachbit

    ## Logiops for Logitech MX master
    logiops

    ## Touch typing tutor
    klavaro
  ];
}
