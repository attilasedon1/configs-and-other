{config, pkgs, ...}:
{
  imports = [ ./hardware-configuration.nix ];
  networking.hostName = "X230";
  system.stateVersion = "24.05";
}
