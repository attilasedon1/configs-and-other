{config, pkgs, ...}:
 {
   imports = [ ./hardware-configuration.nix ];
   boot.initrd.systemd.dbus.enable = true;
   networking.hostName = "server";

 services.openssh = {
   enable = true;
   settings.PasswordAuthentication = true;
 };


 services.grafana = {
   enable = true;
   settings.server = {
     enable = true;
     domain = "localhost";
     protocol = "http";
     http_port = 2342;
     http_addr = "";
     dataDir = "var/lib/garafana";
   };
 };
  services.prometheus = {
   enable = true;
   port = 9001;
   exporters = {
     node = {
       enable = true;
       enabledCollectors = [ "systemd" ];
       port = 9002;
     };
   };
   scrapeConfigs = [
     {
       job_name = "sedon";
       static_configs = [{
         targets = [ "127.0.0.1:${toString config.services.prometheus.exporters.node.port}" ];
       }];
     }
   ];
 }; 

 networking.firewall.allowedTCPPorts = [
   22 # ssh
   2342 # grafana
   9001 # prometheus
   3389 # XRDP
];
 networking.interfaces.enp3s0.ipv4.addresses = [{ address = "192.168.0.102"; prefixLength = 24; }];
 networking.defaultGateway = "192.168.0.1";
 networking.nameservers = ["9.9.9.9"];


services.xrdp = {
  enable = true;
  defaultWindowManager = "startplasma-x11";
  openFirewall = true;
};

users.users.cloudflared = {
  group = "cloudflared";
  isSystemUser = true;
};
users.groups.cloudflared = { };

# This is impure but it works flawlessly
systemd.services.ssh-tunnel = {
  wantedBy = [ "multi-user.target" ];
  after = [ "network.target" "network-online.target" "systemd-resolved.service" ];
  serviceConfig = {
    ExecStart = ''
      ${pkgs.cloudflared}/bin/cloudflared tunnel --no-autoupdate run --token ${builtins.readFile /etc/nixos/cloudflare-ssh}
      '';
    Restart = "always";
    User = "cloudflared";
    Group = "cloudflared";
  };
};


systemd.services.server-tunnel = {
  wantedBy = [ "multi-user.target" ];
  after = [ "network.target" "network-online.target" "systemd-resolved.service" ];
  serviceConfig = {
    ExecStart = ''
      ${pkgs.cloudflared}/bin/cloudflared tunnel --no-autoupdate run --token ${builtins.readFile /etc/nixos/cloudflare-token}
      '';
    Restart = "always";
    User = "cloudflared";
    Group = "cloudflared";
  };
};

systemd.services.rdp-tunnel = {
  wantedBy = [ "multi-user.target" ];
  after = [ "network.target" "network-online.target" "systemd-resolved.service" ];
  serviceConfig = {
    ExecStart = ''
      ${pkgs.cloudflared}/bin/cloudflared tunnel --no-autoupdate run --token ${builtins.readFile /etc/nixos/cloudflare-rdp}
      '';
    Restart = "always";
    User = "cloudflared";
    Group = "cloudflared";
  };
};
system.stateVersion = "23.05";
 }
