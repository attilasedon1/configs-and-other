(print "Started Emacs Install Script")

;; Setup use-package
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                             ("org" . "https://orgmode.org/elpa/")
                             ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents (package-refresh-contents))

;; initilize use-package on non linux platforms
(unless (package-installed-p 'use-package) (package-install 'use-package))

;; Always ensure that the package will be available (download it if it is not available)
(require 'use-package)
(setq use-package-always-ensure t)
(setq use-package-verbose t)


(use-package org-pdftools)
(funcall (lambda() (print "PDF tools is installing") (pdf-tools-install)))

(use-package all-the-icons)
(funcall (lambda() (print "all-the-icons are installing")(all-the-icons-install-fonts)))
