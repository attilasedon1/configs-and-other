#!/usr/bin/env bash

configs_folder=/home/sedon/Projects/configs-and-other
scripts_folder=$configs_folder/scripts

# Script
read  -p "Did you setup this system's flake?" input
cd ~
echo "Installing git and xclip for copying"
nix-env -iA nixos.git nixos.xclip
echo "Installed git. Generating SSH key"
ssh-keygen -t ed25519 -C "$1"
echo "Generated SSH key"
xclip -sel clip < ~/.ssh/id_ed25519.pub
echo "Copied SSH key to clipboard, add this to your Gitlab."
read  -p "After you do write 'yes' here:" input
echo "$input"
while [ "$input" != "yes" ]; do
    read -p "Input is not valid, please write 'yes' here:" input
done
echo "Cloning Configs, Tasks and Knowledge Base"
mkdir ~/Projects
cd ~/Projects
git clone git@gitlab.com:attilasedon/configs-and-other.git
cd ~
echo "Add .zshrc to suppress zsh warning message"
touch .zshrc
echo "Installing Emacs"
nix-env -iA nixos.emacs
echo "Installed Emacs. Running Emacs script to setup configurations"
emacs --no-site-file --script $scripts_folder/autoinstall-tangle.el
# Save hardware config
echo "Save hardware config of the system"
cp /etc/nixos/hardware-configuration.nix $configs_folder/flake/$1/
echo "Install NixOS from flake configuration"
cd ~/Projects/configs-and-other
git add .
echo "Save hardware config of the system to git"
git commit -m "Saved hardware config of $1"
git push
cd ~
# Impurity should be fixed, but right now it's fine
sudo nixos-rebuild switch --flake "$configs_folder/flake#$1" --impure
echo "Run Emacs Install script"
emacs --script $scripts_folder/autoinstall-emacs.el
echo "Cleanup Nix Environment"
nix-env -e git emacs xclip
echo "Installation finished from this script"
echo "Also don't forget in Firefox to enable DRM(Yuck!), add Netflux 1080p extension and check Ctrl-Alt-Shift-D to check the bitrate"
echo "Add Bitwarden, setup Nextcloud"
echo "Don't forget add 'Emacs (Client)' and 'Nextcloud' to startup applications"
