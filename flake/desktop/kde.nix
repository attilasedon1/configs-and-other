{config, pkgs, ...}:
{
  environment.systemPackages = with pkgs; [
    kdePackages.ark
    kdePackages.plasma-browser-integration
    kdePackages.ktorrent
    kdePackages.kalk
    transmission_4-gtk
  ];

  services = {
    displayManager = {
      sddm.enable = true;
    };
    desktopManager.plasma6.enable = true;
  };

}
