{ config, pkgs, host, userSedon, ... }:
  {
  # Bootloader
    boot.kernelPackages = pkgs.linuxPackages_latest;
    boot = {
      loader = {
        efi = {
          canTouchEfiVariables = true;
        };
        grub = {
          enable = true;
          efiSupport = true;
          devices = [ "nodev" ];
          # Limit generations
          configurationLimit = 5;
        };
      };
    };

  # Enable networking
  networking.networkmanager.enable = true;
  networking.firewall.enable = true;                                 
  networking.firewall.allowedTCPPorts = [
    #8081
    #9090
  ];

  # Set your time zone.
  time.timeZone = "Europe/Budapest";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.utf8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "hu_HU.utf8";
    LC_IDENTIFICATION = "hu_HU.utf8";
    LC_MEASUREMENT = "hu_HU.utf8";
    LC_MONETARY = "hu_HU.utf8";
    LC_NAME = "hu_HU.utf8";
    LC_NUMERIC = "hu_HU.utf8";
    LC_PAPER = "hu_HU.utf8";
    LC_TELEPHONE = "hu_HU.utf8";
    LC_TIME = "en_US.utf8";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  hardware.bluetooth = {
    enable = true;
    settings.General.Enable = "Source,Sink,Media,Socket";
  };
  hardware.graphics.enable = true;

  users.users.sedon = {
    isNormalUser = true;
    description = "Attila Sedon";
    extraGroups = [ "networkmanager" "wheel" "video" "libvirt" "kvm" "libvirtd"];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  environment = {
    systemPackages = with pkgs; [
      # System utilities
      wirelesstools
      coreutils
      pciutils
      lsof

      # editors
      vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.

      # terminal
      zsh
      oh-my-zsh
      cool-retro-term

      # terminal commands
      wget
      gnutar
      ripgrep
      btop
      nvtopPackages.full
      bat
      lsd
      du-dust
      fd
      # tldr
      tealdeer
      duf
      gping
      fzf
      findutils
      ranger
      ntfs3g
      tmux
      pandoc
      unzip
      wl-clipboard
      tree

      # Power stuff
      powertop
      upower

      # Development tools 
      git
      git-lfs
      gcc
      gmp
      gnumake

      ## Antivirus
      clamav

      ## Appimage stuff
      fuse #needed to run appimages
      appimage-run
    ];
   };

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    ohMyZsh.theme = "lambda";
    ohMyZsh.enable = true;
    ohMyZsh.plugins = [ "git" ];
    autosuggestions.enable = true;
    syntaxHighlighting.enable = true;
    shellAliases = {
      update = "sudo nixos-rebuild switch --flake '/home/${userSedon}/Projects/configs-and-other/flake#${host.hostName}' --impure";
      ls = "lsd";
      cat = "bat";
      df = "duf";
      du = "dust";
    };
  };
  programs.vim.enable = true;
  programs.vim.defaultEditor = true;

  services = {
    # Enable touchpad support (enabled default in most desktopManager).
    libinput.enable = true;
    xserver = {
      enable = true;
      xkb = {
        layout = "hu,gb";
        options = "grp:win_space_toggle,caps:escape_shifted_capslock,lv3:alt_switch";
        variant = ",mac";
      };
    };
  };
  console.keyMap = "hu101";

  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = false;

  users.defaultUserShell = pkgs.zsh;

  virtualisation.libvirtd.enable = true;
  virtualisation.docker.enable = true;

  nix = {
    package = pkgs.nixVersions.stable;
    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
    };
    gc = {
      automatic = true; dates = "weekly";
    };
  };
}
