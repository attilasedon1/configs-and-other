;; This is an operating system configuration generated
;; by the graphical installer.
;;
;; Once installation is complete, you can learn and modify
;; this file to tweak the system configuration, and pass it
;; to the 'guix system reconfigure' command to effect your
;; changes.


;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu) (nongnu packages linux) (nongnu system linux-initrd) (gnu packages shells))
(use-service-modules cups desktop networking ssh xorg)

(operating-system

  (locale "en_US.utf8")
  (timezone "Europe/Budapest")
  (keyboard-layout (keyboard-layout "hu,gb" ",mac" #:options '("grp:win_space_toggle" "caps:escape_shifted_capslock")))
  (host-name "medic")
  (kernel linux)
  (initrd microcode-initrd)
  (firmware (list linux-firmware))

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                  (name "sedon")
                  (comment "Attila Sedon")
                  (group "users")
		              (shell (file-append fish "/bin/fish"))
                  (home-directory "/home/sedon")
                  (supplementary-groups '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))

  ;; Packages installed system-wide.  Users can also install packages
  ;; under their own account: use 'guix search KEYWORD' to search
  ;; for packages and 'guix install PACKAGE' to install a package.
  (packages (append (map specification->package (list 
						  "emacs" 
						  "neovim" 
						  "ungoogled-chromium-wayland" 
						  "firefox" 
						  "cool-retro-term" 
						  "fish"
              "tmux"
						  "steam" 
						  "btop" 
						  "nextcloud-client" 
						  "gnome-shell-extension-appindicator" 
						  "git" 
						  "font-jetbrains-mono"
						      ))
                    %base-packages))

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services
    
   (append (list (service gnome-desktop-service-type)

                 ;; To configure OpenSSH, pass an 'openssh-configuration'
                 ;; record as a second argument to 'service' below.
                 (service openssh-service-type)
                 (service cups-service-type)
                 (set-xorg-configuration
                  (xorg-configuration (keyboard-layout keyboard-layout))))

           ;; This is the default list of services we
           ;; are appending to.
(modify-services %desktop-services
             (guix-service-type config => (guix-configuration
               (inherit config)
               (substitute-urls
                (append (list "https://substitutes.nonguix.org")
                  %default-substitute-urls))
               (authorized-keys
                (append (list (local-file "./signing-key.pub"))
                  %default-authorized-guix-keys)))))
           ))
  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))
  (swap-devices (list (swap-space
                        (target (uuid
                                 "17235a34-74d4-4db5-8f7a-2b62a821ba5d")))))

  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (file-systems (cons* (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "F334-D109"
                                       'fat32))
                         (type "vfat"))
                       (file-system
                         (mount-point "/")
                         (device (uuid
                                  "286f5c8b-b716-46f5-8005-80bbada6d7de"
                                  'ext4))
                         (type "ext4")) %base-file-systems)))
