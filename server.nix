# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.initrd.systemd.dbus.enable = true;
  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  # networking.networkmanager.enable = true;

  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = true;
  };


  services.grafana = {
    enable = true;
    settings.server = {
      enable = true;
      domain = "localhost";
      protocol = "http";
      http_port = 2342;
      http_addr = "";
      dataDir = "var/lib/garafana";
    };
  };
   services.prometheus = {
    enable = true;
    port = 9001;
    exporters = {
      node = {
        enable = true;
        enabledCollectors = [ "systemd" ];
        port = 9002;
      };
    };
    scrapeConfigs = [
      {
        job_name = "sedon";
        static_configs = [{
          targets = [ "127.0.0.1:${toString config.services.prometheus.exporters.node.port}" ];
        }];
      }
    ];
  }; 

  networking.firewall.allowedTCPPorts = [
    22 # ssh
    2342 # grafana
    9001 # prometheus
    3389 # XRDP
 ];
  networking.interfaces.enp3s0.ipv4.addresses = [{ address = "192.168.0.102"; prefixLength = 24; }];
  networking.defaultGateway = "192.168.0.1";
  networking.nameservers = ["8.8.8.8"];

  # Set your time zone.
  time.timeZone = "Europe/Budapest";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "hu_HU.UTF-8";
    LC_IDENTIFICATION = "hu_HU.UTF-8";
    LC_MEASUREMENT = "hu_HU.UTF-8";
    LC_MONETARY = "hu_HU.UTF-8";
    LC_NAME = "hu_HU.UTF-8";
    LC_NUMERIC = "hu_HU.UTF-8";
    LC_PAPER = "hu_HU.UTF-8";
    LC_TELEPHONE = "hu_HU.UTF-8";
    LC_TIME = "hu_HU.UTF-8";
  };

  # Configure keymap in X11
  services.xserver = {
    enable = true;
    layout = "hu";
    xkbVariant = "";
    desktopManager.plasma5.enable = true;
    displayManager.sddm.enable = true;
  };

  services.xrdp = {
    enable = true;
    defaultWindowManager = "startplasma-x11";
    openFirewall = true;
  };

  # Configure console keymap
  console.keyMap = "hu101";

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.sedon = {
    isNormalUser = true;
    description = "Attila Sedon";
    extraGroups = [ "networkmanager" "wheel" "audio" ];
    packages = with pkgs; [];
  };

  users.users.cloudflared = {
    group = "cloudflared";
    isSystemUser = true;
  };
  users.groups.cloudflared = { };

  systemd.services.ssh-tunnel = {
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" "network-online.target" "systemd-resolved.service" ];
    serviceConfig = {
      ExecStart = ''
        ${pkgs.cloudflared}/bin/cloudflared tunnel --no-autoupdate run --token ${builtins.readFile /etc/nixos/cloudflare-ssh}
        '';
      Restart = "always";
      User = "cloudflared";
      Group = "cloudflared";
    };
  };

  systemd.services.server-tunnel = {
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" "network-online.target" "systemd-resolved.service" ];
    serviceConfig = {
      ExecStart = ''
        ${pkgs.cloudflared}/bin/cloudflared tunnel --no-autoupdate run --token ${builtins.readFile /etc/nixos/cloudflare-token}
        '';
      Restart = "always";
      User = "cloudflared";
      Group = "cloudflared";
    };
  };
  
  systemd.services.rdp-tunnel = {
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" "network-online.target" "systemd-resolved.service" ];
    serviceConfig = {
      ExecStart = ''
        ${pkgs.cloudflared}/bin/cloudflared tunnel --no-autoupdate run --token ${builtins.readFile /etc/nixos/cloudflare-rdp}
        '';
      Restart = "always";
      User = "cloudflared";
      Group = "cloudflared";
    };
  };

  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.tcp.enable = true;

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
   vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
   cloudflared
   git
  #  wget
  ];
  nix = { package = pkgs.nixFlakes; settings = { auto-optimise-store =
    true; experimental-features = [ "nix-command" "flakes" ]; }; gc = {
    automatic = true; dates = "weekly"; options = "--delete-older-than
    7d"; }; };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
